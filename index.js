'use strict';
const _ = require('lodash');
const compose = require('compose-middleware').compose;

const populateWithArguments = (mongoose, idPath, modelName, resultPath) => (req, res, next) => {
  // check if object already exists
  if (_.get(req, resultPath)) {
    return next();
  }
  // determine the relevant id
  const queryId = _.get(req, ['query', idPath]);
  const paramsId = _.get(req, ['params', idPath]);
  const bodyId = _.get(req, ['body', idPath]);
  const docId = queryId || paramsId || bodyId;
  if (!docId) {
    return next();
  }
  const model = mongoose.model(modelName);
  return model.findById(mongoose.Types.ObjectId(docId)).exec()
    .then(doc => {
      if (!doc) {
        return Promise.resolve(next());
      }
      _.set(req, resultPath, doc);
      return Promise.resolve(next());
    }).catch(err => {
      _.set(req, resultPath, null);
      return Promise.resolve(next());
    });
};

const populateWithObject = (mongoose, obj) => {
  const idPath = obj.idPath;
  const modelName = obj.modelName;
  const resultPath = obj.resultPath;
  return populateWithArguments(mongoose, idPath, modelName, resultPath);
};

// Note that we must use 'function' here to get access to 'arguments'
const populate = function () {
  const numArgs = arguments.length;
  if (numArgs === 2) {
    return populateWithObject.apply(null, arguments);
  } else {
    return populateWithArguments.apply(null, arguments);
  }
};

const multiPopulate = (mongoose, list) => compose(_.map(list, entry => {
  const args = entry.length ? [mongoose].concat(entry) : [mongoose, entry];
  return populate.apply(null, args);
}));

module.exports = {
  populate,
  multiPopulate,
};
